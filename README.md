# COVID-19 Growth factors

## Growth Factor for daily reported deaths and cases
#### Note on China:
According to `worldometers.info`:

> #### April 17
> China: Hubei Province issued today a "Notice on the
> Correction of the Number of New Coronary Pneumonia Cases
> Diagnosed and the Number of Diagnosed Deaths in Wuhan" in
> which it reported 1,290 additional deaths that had not been
> previously counted and reported, bringing the total number
> of deaths in Wuhan from 2,579 to 3,869, an increase of 50%,
> as the result of a revision by the Wuhan New Coronary
> Pneumonia Epidemic Prevention and Control. As part of this
> revision, 325 additional cases in Wuhan were also added.
> Separately, China's National Health Commission (NHC)
> reported 26 new cases (and no deaths) in its daily report.

ECDC dataset is amended to include only NHC's daily report.

### China
![plot9](./img/gd_deathsNcases_China.svg)

### Italy
![plot11](./img/gd_deathsNcases_Italy.svg)

### Spain
![plot13](./img/gd_deathsNcases_Spain.svg)

### UK
![plot15](./img/gd_deathsNcases_UK.svg)

### USA
![plot16](./img/gd_deathsNcases_USA.svg)

### Germany
![plot17](./img/gd_deathsNcases_Germany.svg)

## Effect of varying Ν

### China
![plot44](./img/gd_deaths_fitww_China.svg)

### Italy
![plot45](./img/gd_deaths_fitww_Italy.svg)

