
#set ylabel "Days to grow by 1000" offset +1,0;
#set y2label "λ_n" offset -3,0
#
#set y2tics mirror
#
#set ytics add ( "" 1 )
#do for [it = 1:20:1] {
#    lf = 1+0.10*it;
#    ls = sprintf("%.0f",  log(1000.0)/log(lf));
#    set ytics add ( ls lf );
#}


set grid
set grid mxtics

set xdata time
set timefmt "%d/%m/%Y"


set key horizontal top noenhanced
set lmargin at screen 0.08
set rmargin at screen 0.92
