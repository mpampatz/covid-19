# run in batch mode:
# gnuplot -d -p -e "OUTSVG='./img/tst'; COUNTRIES='Bulgaria Turkey'" -c 'src/gd_deaths.gp'


reset
load 'src/tools.gp'
load 'src/macro.gp'
load 'src/cfg.gp'


# ".svg" will get appended with `SVG_MAKENAME`
if ( !exists("OUTSVG") ) {
    OUTSVG = "./img/gd_deaths_fitww"
}
if ( !exists("COUNTRIES") ) {
    COUNTRIES = "Italy"
}
FITWW = 9;
col1 = "d_deathsRateWLS"

#NOTE: title shows FITWW and σ
set title "Effect of varying rolling sample size N on fitted Growth Factor\nof daily reported deaths of ".COUNTRIES."."
unset y2tics
unset y2label
unset ytics
set ytics
set xrange [ * : time(1) ]
set yrange [0.75:1.6]


@SVG_MAKENAME
@IF_BATCH_INIT

plot \
    @PL_WLCOLOR_RESET, \
    for [ FITWW = 13:1:-4 ] ASK(FITWW,COUNTRIES) \
        using @PL_TIMESHIFT:( column(col1) )\
        title "N=".FITWW\
        @PL_SMOOTH \
        @PL_WLCOLOR_NEXT

@IF_BATCH_FINIT

