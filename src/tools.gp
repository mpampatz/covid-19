
ASK(fww, country) = '< awk -v fitww='.fww.' -v country='.country.' -f ./src/extract.awk ./csv/data.csv'

# hacks to control automatic color rotation
iautocolor=0
nextcolor(s) = ( iautocolor=iautocolor+s, iautocolor )
resecolor(s) = ( iautocolor=0, iautocolor )


