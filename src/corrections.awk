BEGIN {
    FS = ",";
    OFS = ",";
}

/^18\/04\/2020,18,4,2020,0,0,Greece,EL,GRC,10727668,Europe\r/ {
    $5 = 17;
    $6 = 3;
    print;
    next;
}

/^19\/04\/2020,19,4,2020,0,0,Greece,EL,GRC,10727668,Europe\r/ {
    $5 = 11;
    $6 = 2;
    print;
    next;
}

/^20\/04\/2020,20,4,2020,28,5,Greece,EL,GRC,10727668,Europe\r/ {
    $5 = 0;
    $6 = 3;
    print;
    next;
}

/^21\/04\/2020,21,4,2020,10,6,Greece,EL,GRC,10727668,Europe\r/ {
    $6 = 3;
    print;
    next;
}

/^17\/04\/2020,17,4,2020,352,1290,China,CN,CHN,1392730000,Asia\r/ {
    $5 = 26;
    $6 = 0;
    print;
    next;
}

{
    print;
}

