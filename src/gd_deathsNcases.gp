# run in batch mode:
# gnuplot -d -p -e "OUTSVG='./img/tst'; COUNTRIES='Bulgaria Turkey'" -c 'src/gd_deaths.gp'


reset
load 'src/tools.gp'
load 'src/macro.gp'
load 'src/cfg.gp'


# ".svg" will get appended with `SVG_MAKENAME`
if ( !exists("OUTSVG") ) {
    OUTSVG = "./img/gd_deathsNcases"
}
if ( !exists("COUNTRIES") ) {
    COUNTRIES = "China Italy USA"
}
FITWW = 9;
col1 = "d_deathsRateWLS"
col2 = "d_casesRateWLS"

set title "{Growth Factor of daily reported deaths and cases}\n{/*0.75 Method: ".FITWW." days rolling log-linear least squares with gaussian weights. Computed values at center of rolling sample.}"
unset y2tics
unset y2label
unset ytics
set ytics
set xrange [ * : time(1) ]


@SVG_MAKENAME
@IF_BATCH_INIT

plot \
    keyentry title "Deaths" w l lc 'black' lw 1.5,\
    keyentry title "Cases" w l lc 'black' lw 1.5 dt 2,\
    @PL_WLCOLOR_RESET, \
    for [ c in COUNTRIES ] ASK(FITWW,c) \
        using @PL_TIMESHIFT:( column(col1) )\
        title c\
        @PL_SMOOTH \
        @PL_WLCOLOR_NEXT, \
    @PL_WLCOLOR_RESET,\
    for [ c in COUNTRIES ] ASK(FITWW,c) \
        using @PL_TIMESHIFT:( column(col2) )\
        notitle \
        @PL_SMOOTH \
        @PL_WLCOLOR_NEXT dt 2

@IF_BATCH_FINIT

