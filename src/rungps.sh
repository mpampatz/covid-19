#!/usr/bin/bash


function gd_deathsNcases() {
    gnuplot -d -p -e "COUNTRIES='$1'" -c 'src/gd_deathsNcases.gp' -b
}

function gd_deaths() {
    gnuplot -d -p -e "COUNTRIES='$1'" -c 'src/gd_deaths.gp' -b
}

function gc_deaths() {
    gnuplot -d -p -e "COUNTRIES='$1'" -c 'src/gc_deaths.gp' -b
}

function gcvsd_deaths() {
    gnuplot -d -p -e "COUNTRIES='$1'" -c 'src/gcvsd_deaths.gp' -b
}

function gd_deaths_fitww() {
    gnuplot -d -p -e "COUNTRIES='$1'" -c 'src/gd_deaths_fitww.gp' -b
}


gd_deathsNcases Italy
gd_deathsNcases USA
gd_deathsNcases China
gd_deathsNcases Greece
gd_deathsNcases Spain
gd_deathsNcases UK
gd_deathsNcases Netherlands
gd_deathsNcases Sweden
gd_deathsNcases Germany

gd_deaths 'Italy UK USA China'
gd_deaths 'Greece Bulgaria Italy Spain UK North_Macedonia France USA'
gd_deaths 'Greece Italy Turkey' 

gc_deaths 'Italy UK USA China'
gc_deaths 'Greece Bulgaria Italy Spain UK North_Macedonia France USA'
gc_deaths 'Greece Italy Turkey Germany'

gcvsd_deaths 'Italy UK USA China'
gcvsd_deaths 'Greece Italy Turkey Germany'

gd_deaths_fitww Italy
gd_deaths_fitww China
