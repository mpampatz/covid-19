
PL_SMOOTH = "smooth mcsplines"
PL_WLCOLOR_AUTO = "with lines linewidth 1.5"
PL_WLCOLOR_NEXT = "with lines linestyle nextcolor(1) linewidth 1.5"
PL_WLCOLOR_RESET = "0/0 notitle with lines linestyle resecolor(0)"
PL_TIME = "(timecolumn(1,'%d/%m/%Y'))"
PL_TIMESHIFT = "(timecolumn(1,'%d/%m/%Y')-12*60*60*FITWW)"

SVG_BG_COLOR = "#e6e0e1"
SVG_MAKENAME = "do for [ c in COUNTRIES ] { OUTSVG = OUTSVG.'_'.c }; OUTSVG = OUTSVG.'.svg'"
IF_BATCH_INIT = "if ( exists('ARG1') && ARG1 eq '-b' ) set terminal svg dynamic background SVG_BG_COLOR; set output OUTSVG;"
IF_BATCH_FINIT = "if ( exists('ARG1') && ARG1 eq '-b' ) set output;"





