#!/usr/bin/bash

WORKING_DIR=$(basename ${PWD});
URL="https://opendata.ecdc.europa.eu/covid19/casedistribution/csv";
OUTPUT="./csv/data.csv"


if [ $WORKING_DIR != "opendata" ]; then
    exit 1;
fi

curl -L $URL -w "\n" | sed -e '1d' | tac | awk -f "./src/corrections.awk" > $OUTPUT;
#curl -L $URL -w "\n" | head





