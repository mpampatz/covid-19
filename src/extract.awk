# csv file headline for reference:
# "dateRep,day,month,year,cases,deaths,countriesAndTerritories,geoId,countryterritoryCode,popData2018"

# INPUT:
#   integer     fitww    :   fitting sample size
#   string      country     :   example: any of Zimbabwe, ziMbaBwe, ZW, zW or ZWE finds Zimbabwe
BEGIN {
    FS = ",";
    OFS = "\t";
    IGNORECASE = 1;

    c_cases = 0;
    c_deaths = 0;

    index_1st_case = 0;
    index_1st_death = 0;

    _found = 0;
    _countryERE = sprintf("\\<%s\\>",country);

    c_casesRate = "NaN";
    c_casesRateLS = "NaN";
    c_casesRateWLS = "NaN";
    d_casesRateLS = "NaN";
    d_casesRateWLS = "NaN";

    c_deathsRate = "NaN";
    c_deathsRateLS = "NaN";
    c_deathsRateWLS = "NaN";
    d_deathsRateLS = "NaN";
    d_deathsRateWLS = "NaN";

    for (i=0; i<=fitww; i++) {
        c_casesQue[i] = 1;
        c_deathsQue[i] = 1;
        d_casesQue[i] = 1;
        d_deathsQue[i] = 1;

        #a gaussian filter
        filter[i] = exp(-0.5*( (0.5*fitww-i)/( 0.33*fitww ) )^2)
    }
    print "dateRep", \
"d_cases",  "c_cases",  "c_casesRate",  "c_casesRateLS",  "c_casesRateWLS",  "d_casesRateLS",  "d_casesRateWLS", \
"d_deaths", "c_deaths", "c_deathsRate", "c_deathsRateLS", "c_deathsRateWLS", "d_deathsRateLS", "d_deathsRateWLS";
}

{
    if ( !match( $7, _countryERE ) &&
         !match( $8, _countryERE ) &&
         !match( $9, _countryERE ) ) {
        if ( _found == 0 )
            next
        else
            exit;
    }
    _found = 1;

    dateRep = $1;

    d_cases = $5;
    d_deaths = $6;

    if ( !index_1st_case && d_cases > 0 ) index_1st_case = NR;
    if ( !index_1st_death && d_deaths > 0 ) index_1st_death = NR;

    c_cases += d_cases;
    c_deaths += d_deaths;

    queInsert( c_deathsQue, fitww, c_deaths );
    queInsert( c_casesQue, fitww, c_cases );
    queInsert( d_casesQue, fitww, d_cases );
    queInsert( d_deathsQue, fitww, d_deaths );

    if ( index_1st_case && (NR - index_1st_case) > (fitww/2) ) {
        c_casesRate = calcFactorA( c_casesQue[0], c_casesQue[fitww], fitww, 0 );
        c_casesRateLS = calcFactorLS( c_casesQue, fitww );
        c_casesRateWLS = calcFactorWLS( c_casesQue, fitww );
        d_casesRateLS = calcFactorLS( d_casesQue, fitww );
        d_casesRateWLS = calcFactorWLS( d_casesQue, fitww );
    }

    if ( index_1st_death  && (NR - index_1st_death) > (fitww/2) ) {
        c_deathsRate = calcFactorA( c_deathsQue[0], c_deathsQue[fitww], fitww, 0 );
        c_deathsRateLS = calcFactorLS( c_deathsQue, fitww );
        c_deathsRateWLS = calcFactorWLS( c_deathsQue, fitww );
        d_deathsRateLS = calcFactorLS( d_deathsQue, fitww );
        d_deathsRateWLS = calcFactorWLS( d_deathsQue, fitww );
    }

    if ( c_deaths < 1 ) next;

    print dateRep, \
d_cases,  c_cases,  c_casesRate,  c_casesRateLS,  c_casesRateWLS,  d_casesRateLS,  d_casesRateWLS, \
d_deaths, c_deaths, c_deathsRate, c_deathsRateLS, c_deathsRateWLS, d_deathsRateLS, d_deathsRateWLS;
}

END {
    if (_found != 1) exit 1;
}

# Conservative calculation of growth factor.
# y_n = y_i * omega^(n-i). Solve for omega.
function calcFactorA( yn, yi, n, i ) {
    if ( yn <= 0 || yi <= 0 ) return "-";
    return (yn/yi)^(1/(n-i));
}

# Least Square calculation of growth factor.
# Involves all internal data `yᵢ`.
# Given than the sequence of independent variables `i`
# is a set of consecutive integers...
#
#            2·∑i·ln(yᵢ) - n·∑ln(yᵢ)
#   ln(ω) = ─────────────────────────
#                n·(n+1)·(n+1)/6
#
function calcFactorLS( que, nque ) {
    Syi = 0;
    Siyi = 0;
    for ( i=nque; i>=0; i-- ) {
        Syi += log(que[i]);
        Siyi += (nque-i)*log(que[i]);
    }

    lnomega = 6 * ( 2*Siyi - nque*Syi ) / ( nque*(nque+1)*(nque+2) );
    ret = exp(lnomega);
    return ret ; #( ret !~ "-?nan" ? ret : 1 );
}

function calcFactorWLS( que, nque ) {
    Sii = 0;
    Si = 0;
    S = 0;
    Syi = 0;
    Siyi = 0;
    for ( i=nque; i>=0; i-- ) {
        S   += filter[i] * 1;
        Si  += filter[i] * (nque-i);
        Sii += filter[i] * (nque-i)^2;
        Syi += filter[i] * log(que[i]);
        Siyi+= filter[i] * (nque-i)*log(que[i]);
    }

    lnomega = ( S * Siyi - Si * Syi ) / ( S * Sii - Si * Si )
    ret = exp(lnomega);
    return ret ;
}

function queInsert( que, nque, element ) {
    for ( i=nque; i>0; i-- )
        que[i] = que[i-1];
    que[i] = ( element < 1 ) ? 1 : element;
}


