# run in batch mode:
# gnuplot -d -p -e "OUTSVG='./img/tst'; COUNTRIES='Bulgaria Turkey'" -c 'src/gd_deaths.gp'


reset
load 'src/tools.gp'
load 'src/macro.gp'
load 'src/cfg.gp'


# ".svg" will get appended with `SVG_MAKENAME`
if ( !exists("OUTSVG") ) {
    OUTSVG = "./img/gc_deaths"
}
if ( !exists("COUNTRIES") ) {
    COUNTRIES = "China Italy Spain USA"
}
FITWW = 9;
col1 = "c_deathsRateWLS"

#NOTE: title shows FITWW and σ
set title "{Growth Factor of cumulative reported deaths}\n{/*0.75 Method: ".FITWW." days rolling log-linear least squares with gaussian weights. Computed values at center of rolling sample.}"
unset y2tics
unset y2label
unset ytics
set ytics
set xrange [ * : time(1) ]


@SVG_MAKENAME
@IF_BATCH_INIT

plot \
    @PL_WLCOLOR_RESET, \
    for [ c in COUNTRIES ] ASK(FITWW,c) \
        using @PL_TIMESHIFT:( column(col1) )\
        title c\
        @PL_SMOOTH \
        @PL_WLCOLOR_NEXT

@IF_BATCH_FINIT

